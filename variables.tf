variable "name" {
  type        = string
  description = "Load balancer name"
}

variable "type" {
  type        = string
  description = "Load balancer type"
  default     = "application"

  validation {
    condition     = var.type == "application" || var.type == "network"
    error_message = "The type must be either \"application\" or \"network\"."
  }
}

variable "vpc_id" {
  type        = string
  description = "VPC identifier."
}
