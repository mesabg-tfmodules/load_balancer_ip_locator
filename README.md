# Load Balancer IP Locator

This module is capable to locate load balancer IP addresses (internal/external).

Module Input Variables
----------------------

- `name` - load balancer name
- `type` - local balancer type
- `vpc_id` - vpc identifier

Usage
-----

```hcl
module "load_balancer_ip_locator" {
  source  = "git::https://gitlab.com/mesabg-tfmodules/load_balancer_ip_locator.git"

  name    = "lb_name"
  type    = "application"
  vpc_id  = "vpc_id"
}
```

Outputs
=======

 - `private_ips` - Private IP addresses


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
