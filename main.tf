data "aws_network_interfaces" "network_interfaces" {
  filter {
    name = "description"
    values = ["ELB ${var.type == "network" ? "net" : "app"}/${var.name}/*"]
  }
  filter {
    name = "vpc-id"
    values = [var.vpc_id]
  }
  filter {
    name = "status"
    values = ["in-use"]
  }
  filter {
    name = "attachment.status"
    values = ["attached"]
  }
}

locals {
  nlb_interface_ids = flatten([data.aws_network_interfaces.network_interfaces.ids != null ? data.aws_network_interfaces.network_interfaces.ids : []])
}

data "aws_network_interface" "ifs" {
  count = length(local.nlb_interface_ids)
  id    = local.nlb_interface_ids[count.index]
}
