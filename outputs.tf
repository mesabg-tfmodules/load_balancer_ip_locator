output "private_ips" {
  value       = flatten([data.aws_network_interface.ifs.*.private_ips])
  description = "ELB Assigned private IPs"
}
